# Awesome list of open source web platforms
If do you care about your freedom to self-host and self-data control, decentralization, transparency,
or just good alternatives.

## Atheos - web-based IDE framework
* [atheos.io](https://www.atheos.io/)
* [github](https://github.com/Atheos/Atheos)

## BookWyrm - Social Reading and Reviewing Books
* [github](https://github.com/bookwyrm-social/bookwyrm)
* [bookwyrm.social](https://bookwyrm.social/)
* [joinbookwyrm.com](https://joinbookwyrm.com/)

## Diaspora - Social
* [diasporafoundation.org](https://diasporafoundation.org/)

## Discourse - Modern Forum
* [github](https://github.com/discourse/discourse)
* [discourse.org](https://www.discourse.org/)

## Ergo - IRC Server
* [github](https://github.com/ergochat/ergo)
* [ergo.chat](https://ergo.chat/)
* [testnet.ergo.chat](https://testnet.ergo.chat/)

## Forem - Forum community dev.to-like
* [github](https://github.com/forem/forem)
* [dev.to](https://dev.to/)

## FreshRSS - RSS
* [freshrss.org](https://www.freshrss.org/)

## Friendica - Social
* [friendi.ca](https://friendi.ca/)

## Funkwhale - Music Sharing
* [funkwhale.audio](https://funkwhale.audio/)

## Gnusocial - Social
* [gnusocial.network](https://gnusocial.network/)

## Gitea - Git Repository
* [gitea.com](https://gitea.com/)
* [gitea.org](https://gitea.org/)

## Gogs - Git Repository
* [gogs.io](https://gogs.io/)

## Gotify - Push Notification
* [github](https://github.com/gotify/server)
* [gotify.net](https://gotify.net/)

## Grafana - Analytics
* [github](https://github.com/grafana/grafana)
* [grafana.com](https://grafana.com/)

## HumHub - Social Network Kit
* [github](https://github.com/humhub/humhub)
* [humhub.com](https://www.humhub.com/en/download)

## Invoiceninja - Invoicing, Quotes, Expenses, Time-Tracking
* [invoiceninja.org](https://invoiceninja.org/)

## Ivatar/Libravatar - Open Avatar Profile
* [git](https://git.linux-kernel.at/oliver/ivatar/)
* [libravatar.org](https://www.libravatar.org/)

## Jitsi - Meeting/Video conferencing 
* [meet.jit.si](https://meet.jit.si/)
* [jitsi.org](https://jitsi.org/)

## LBRY - Video Streaming
* [odysee.com](https://odysee.com/)
* [lbry.com](https://lbry.com/)
* [lbry.tv](https://lbry.tv/)

## Lemmy - Forum reddit-like alternative
* [github](https://github.com/LemmyNet/lemmy)
* [lemmy.ml](https://lemmy.ml/)

## Lufi - Encrypted Upload File Share
* [demo.lufi.io](https://demo.lufi.io/)
* [git](https://framagit.org/fiat-tux/hat-softwares/lufi)

## Mastodon - Social/Microblog
* [mastodon.social](https://mastodon.social/)
* [joinmastodon.org](https://joinmastodon.org/)

## Matomo(Piwik) - Analytics
* [github](https://github.com/matomo-org/matomo)
* [matomo.org](https://matomo.org/)

## Matrix/Element - Protocol/Messenger
* [matrix.org](https://matrix.org/)
* [element.io](https://element.io/get-started)

## MediaWiki - Wiki
* [git](https://phabricator.wikimedia.org/source/mediawiki/)
* [mediawiki.org](https://mediawiki.org/)
* [wikimediafoundation.org](https://wikimediafoundation.org/)

## Misskey - Japanese Social/Microblog
* [join.misskey.page](https://join.misskey.page/en/)

## Moodle - Learning management system
* [github](https://github.com/moodle/moodle)
* [moodle.org](https://moodle.org/)
* [moodle.com](https://moodle.com/getstarted/)

## Nextcloud - Cloud Suite
* [nextcloud.com](https://nextcloud.com/)

## Owncast - Broadcast/Streaming 
* [owncast.online](https://owncast.online/)
* [directory.owncast.online](https://directory.owncast.online/)

## Peertube - Video Streaming
* [joinpeertube.org](https://joinpeertube.org/)

## phpBB - Classic Forum
* [github](https://github.com/phpbb/phpbb)
* [phpbb.com](https://www.phpbb.com/)

## Pixelfed - Photo Sharing
* [pixelfed.org](https://pixelfed.org/)

## Pleroma - Social/Microblog
* [pleroma.social](https://pleroma.social/)

## Rocket.Chat - Messenger/Chat
* [github](https://github.com/RocketChat/Rocket.Chat)
* [rocket.chat](https://rocket.chat/install/)

## XMPP - Open Standard Messaging Protocol
* [xmpp.org](https://xmpp.org/)

## Writefreely - Writing/Blog
* [writefreely.org](https://writefreely.org/)
* [write.as](https://write.as/)

## Zammad - Customer Support/Ticketing solution
* [zammad.org](https://zammad.org/)

## ZeroNet - Network/DeepWeb
* [zeronet.io](https://zeronet.io/)

-----

## Reference Links:
* [nogafam](https://www.nogafam.es/)
* [awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted#readme)
* [fediverse.party](https://fediverse.party/)
* [cloud68.co](https://cloud68.co/instances)
* [streaming-video-with-owncast-on-a-free-oracle-cloud-computer](https://www.artificialworlds.net/blog/2021/01/05/streaming-video-with-owncast-on-a-free-oracle-cloud-computer/)